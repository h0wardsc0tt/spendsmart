<cfsilent>
<cfparam name="URL.pg" default="">
<cfparam name="URL.st" default="">
</cfsilent>
<cfinclude template="./_tmp_HTML_Header.cfm">

<cfswitch expression="#URL.pg#">
	<cfcase value="NewHeadset">
		<cfinclude template="./_tmp_wizard.cfm">
	</cfcase>
	<cfcase value="UpgHeadset">
		<cfinclude template="./_tmp_wizard.cfm">
	</cfcase>
	<cfcase value="NewTimer">
		<cfinclude template="./_tmp_wizard.cfm">
	</cfcase>
	<cfcase value="UpgTimer">
		<cfinclude template="./_tmp_wizard.cfm">
	</cfcase>
	<cfcase value="Cart">
		<cfswitch expression="#URL.st#">
			<cfcase value="Add">
				<cfinclude template="./_tmp_cart_add.cfm">
			</cfcase>
			<cfcase value="Del">
				<cfinclude template="./_tmp_cart_del.cfm">
			</cfcase>
			<cfcase value="Clear">
				<cfinclude template="./_tmp_cart_clr.cfm">
			</cfcase>
			<cfcase value="Qty">
				<cfinclude template="./_tmp_cart_qty.cfm">
			</cfcase>
			<cfcase value="Checkout">
				<cfinclude template="./_tmp_cart_cko.cfm">
			</cfcase>
			<cfcase value="Order">
				<cfinclude template="./_tmp_cart_ord.cfm">
			</cfcase>
			<cfcase value="ReOrder">
				<cfinclude template="./_tmp_cart_reorder.cfm">
			</cfcase>
			<cfdefaultcase>
				<cfinclude template="./_tmp_cart_default.cfm">
			</cfdefaultcase>
		</cfswitch>
	</cfcase>
	<cfcase value="Product">
		<cfinclude template="./_tmp_product_default.cfm">
	</cfcase>
	<cfcase value="Detail">
		<cfinclude template="./_tmp_product_detail.cfm">
	</cfcase>
	<cfcase value="Wishlist">
		<cfswitch expression="#URL.st#">
			<cfcase value="Del">
				<cfinclude template="./_tmp_wishlist_del.cfm">
			</cfcase>
			<cfcase value="DelAll">
				<cfinclude template="./_tmp_wishlist_delall.cfm">
			</cfcase>
			<cfdefaultcase>
				<cfinclude template="./_tmp_product_wishlist.cfm">
			</cfdefaultcase>
		</cfswitch>
	</cfcase>
	<cfcase value="Account">
		<cfinclude template="./_tmp_account_default.cfm">
	</cfcase>
	<cfcase value="Search">
		<cfinclude template="./_tmp_product_search.cfm">
	</cfcase>
	<cfcase value="NoAuth">
		<cfinclude template="./_tmp_no_auth.cfm">
	</cfcase>
	<cfdefaultcase>
		<cfinclude template="./_tmp_default.cfm">
	</cfdefaultcase>
</cfswitch>
	
<cfinclude template="./_tmp_HTML_Footer.cfm">