<cfparam name="ERR" default="#StructNew()#">
<cfparam name="ERR.ErrorFound" default="false">
<cfparam name="ERR.ErrorMessage" default="">

<cfscript>
	SharedSecret = "Fries";//Shared between vendor and manufacturer
	
	inc_REQ = GetHttpRequestData();
	str_XML = inc_REQ.Content.Trim();
</cfscript>

<cfdump var="#str_XML#">

<cfset str_XML = ReplaceNoCase(str_XML, "test=", "")>

<cfdump var="#URLDecode(str_XML)#">
<cfabort>

<cfscript>
	//Write Request To dtbl_Request_Auth
	thisRequest = CreateObject("component", "_cfcs.SessionMgmt");
	TrackRequest = thisRequest.TrackIncomingRequest(XML_Request=str_XML);
	
	if(IsXML(str_XML)) {
		dat_XML = XMLParse(str_XML);
		get_Cred = XmlSearch(dat_XML, "/cXML/Header/Sender/Credential/SharedSecret");
		
		if(ArrayLen(get_Cred)) {
			this_Secret = get_Cred[1].XmlText;
			if(Compare(SharedSecret, this_Secret) NEQ 0) {
				ERR.ErrorFound = true;
				ERR.ErrorMessage = "Invalid User";
			} else {
				CREDS = StructNew();
				
				get_UserFrom = XmlSearch(dat_XML, "/cXML/Header/From/Credential/Identity");
				CREDS.UserFrom = get_UserFrom[1].XmlText;
				get_UserTo = XmlSearch(dat_XML, "/cXML/Header/To/Credential/Identity");
				CREDS.UserTo = get_UserTo[1].XmlText;
				
				get_BuyerCookie = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/BuyerCookie");
				CREDS.BuyerCookie = get_BuyerCookie[1].XmlText;
				get_BrowserFormPost = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/BrowserFormPost/URL");
				CREDS.BrowserFormPost = get_BrowserFormPost[1].XmlText;
				get_Contact = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/Contact/Name");
				CREDS.ContactName = get_Contact[1].XmlText;
				get_Email = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/Contact/Email");
				CREDS.Email = get_Email[1].XmlText;
				
				CREDS.Address = StructNew();
				get_ShipTo = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/ShipTo/Address/PostalAddress/DeliverTo");
				CREDS.Address.ShipTo = get_ShipTo[1].XmlText;
				get_Street1 = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/ShipTo/Address/PostalAddress/Street");
				CREDS.Address.Street1 = get_Street1[1].XmlText;
				get_Street2 = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/ShipTo/Address/PostalAddress/Street");
				CREDS.Address.Street2 = get_Street2[2].XmlText;
				get_City = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/ShipTo/Address/PostalAddress/City");
				CREDS.Address.City = get_City[1].XmlText;
				get_State = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/ShipTo/Address/PostalAddress/State");
				CREDS.Address.State = get_State[1].XmlText;
				get_Zip = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/ShipTo/Address/PostalAddress/PostalCode");
				CREDS.Address.Zip = get_Zip[1].XmlText;
				get_Country = XmlSearch(dat_XML, "/cXML/Request/PunchOutSetupRequest/ShipTo/Address/PostalAddress/Country");
				CREDS.Address.Country = get_Country[1].XmlText;
				
			}
			
		} else {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = "Credentials Not Supplied";
		}
	} else {
		ERR.ErrorFound = true;
		ERR.ErrorMessage = "Bad File Format";
	}
	/*if(NOT ERR.ErrorFound) { //init Session
		thisUser = CreateObject("component", "_cfcs.UserMgmt");
		CreateUser = thisUser.authUser(CREDS);
	}*/
	//tmp = StructClear(CREDS);
</cfscript>

<cfif NOT ERR.ErrorFound>
	<cfif StructKeyExists(COOKIE, "user")>
		<cfset CREDS.User_Cookie = COOKIE.user>
	</cfif>
	
	<cfinvoke component="_cfcs.SessionMgmt" method="authUser" argumentcollection="#CREDS#" returnvariable="thisUser"></cfinvoke>
	<cfif StructClear(CREDS)></cfif>
	
	<cfscript>
		UserCookieExpires = DateAdd("m", 6, Now());
		SessCookieExpires = DateAdd("s", 10, Now());
	</cfscript>
	<cfif thisUser.OKGO>
		<cfscript>
			CreateSession = CreateObject("component", "_cfcs.SessionMgmt");
			SESSION_UID = CreateSession.TrackSession(Session_UID=thisUser.Session_UID,User_Cookie=thisUser.User_Cookie);
		</cfscript>
	<cfelse>
		<cfset ERR.ErrorFound = true>
		<cfset ERR.ErrorMessage = "User credentials not valid">
		<cfset URL.pg = "NoAuth">
	</cfif>
</cfif>

<cfsavecontent variable="SPS_Response">
<cfoutput><cfif NOT ERR.ErrorFound><cfset redir = "#APPLICATION.rootURL#?suid=#thisUser.Session_UID#"><?xml version="1.0"?><!DOCTYPE cXML SYSTEM "http://xml.cxml.org/schemas/cXML/1.1.008/cXML.dtd"><cXML version="1.0" payloadID="#DateFormat(Now())# #TimeFormat(Now())#@#CGI.REMOTE_ADDR#" timestamp="#TimeFormat(Now())#">
<Response>
<Status code="200" text="Success"></Status><PunchOutSetupResponse><StartPage>
<URL>#redir#</URL>
</StartPage></PunchOutSetupResponse></Response></cXML>
<cfelse><?xml version="1.0"?><!DOCTYPE cXML SYSTEM "http://xml.cxml.org/schemas/cXML/1.1.008/cXML.dtd"><cXML version="1.0" payloadID="#Now()# #cgi.path_info#" timestamp="#TimeFormat(Now())#">
<Response><Status code="500" Text="#ERR.ErrorMessage#"/></Response></cXML></cfif></cfoutput>
</cfsavecontent>
<cfcontent
	type="text/xml"
	variable="#ToBinary(ToBase64(SPS_Response))#"
	/>
