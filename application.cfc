<cfcomponent
	displayname="Application"
	output="true"
	hint="Handle the application.">

	<!--- Set up the application. --->
	<cfscript>
		THIS.Name = Hash(getCurrentTemplatePath());
		THIS.ApplicationTimeout = CreateTimeSpan(0,0,20,0);
		THIS.SessionTimeout = CreateTimeSpan(0,0,20,0);
		THIS.SessionManagement = true;
		THIS.SetClientCookies = false;
	</cfscript>
	
	<!--- Define the page request properties. --->
	<cfsetting
		requesttimeout="20"
		showdebugoutput="true"
		enablecfoutputonly="false"/>
	<!---Enables Sessions Across Frames--->
	<cfheader name="P3P" value='CP="CAO PSA OUR"'>

	<cffunction
		name="OnApplicationStart"
		access="public"
		returntype="boolean"
		output="false"
		hint="Fires when the application is first created.">
		
		<cfscript>
			APPLICATION.Datasource = "db_spendsmart";
			
			//Change vars based on environment
			//APPLICATION.rootDir = "D:\inetpub\_dev\devspendsmart";
			APPLICATION.rootDir = getDirectoryFromPath(getCurrentTemplatePath());
			APPLICATION.rootURL = "https://#CGI.HTTP_HOST#";
			APPLICATION.compDir = APPLICATION.rootDir & "_cfcs";
			APPLICATION.ApplicationTimeout = THIS.ApplicationTimeout;
			APPLICATION.SessionTimeout = THIS.SessionTimeout;
			
			THIS.mappings = StructNew();
			THIS.mappings["/spendsmart"] = APPLICATION.rootDir;
			
			SITE = StructNew();
			SITE.AdminEmail = "atnelson@hme.com";
			SITE.ITRequest = "ITR@hme.com";
			SITE.MaintUser_IP = "192.168.106.232";
			SITE.IsMaintenance = false; //for putting site into maint mode
			SITE.IsDebug = false; //for putting the site into debug mode
		</cfscript>
	
	<!--- Return out. --->
		<cfreturn true />
	</cffunction>

	<cffunction
		name="OnRequestStart"
		access="public"
		output="true"
		hint="Fires pre page processing.">

		<cfscript>
			ERR = StructNew();
			ERR.ErrorFound = false;
			ERR.ErrorMessage = "";
		</cfscript>
		
		<cfif (Left(GetFileFromPath(CGI.SCRIPT_NAME),1) IS "_")>
			<!---Need to write a catch block in case a user is trying to browse templates--->
			<cfabort>
		</cfif>
		
		<cfif StructKeyExists(URL, "reset")>
			<cfif StructClear(SESSION)></cfif>
			<cfset THIS.OnApplicationStart()>
		</cfif>
		
		<cfif NOT FindNoCase("/admin", CGI.PATH_INFO)>
			<cfif StructKeyExists(URL, "suid") AND NOT StructKeyExists(SESSION, "Session_UID")>
				<cfscript>
					//This Switch should only be hit on first page view after authentication
					thisSession = CreateObject("component", "_cfcs.SessionMgmt");
					VerifySession = thisSession.veriSession(Session_UID=URL.suid);//session validation on DTS <= 20mins
				</cfscript>
			<cfelseif StructKeyExists(SESSION, "Session_UID")>
				<cfscript>
					//This Switch will be hit on all request after inital validation
					thisSession = CreateObject("component", "_cfcs.SessionMgmt");
					VerifySession = thisSession.veriSession(Session_UID=SESSION.Session_UID);//session validation on DTS <= 20mins
				</cfscript>
			<cfelse>
				<cfscript>
					URL.pg = "NoAuth";
					VerifySession = false;
				</cfscript>
			</cfif>
			
			<cfif NOT VerifySession><!---if session fails make user relogin--->
				<cfscript>
					URL.pg = "NoAuth";
					VerifySession = false;
					//x = StructClear(SESSION);//clear any session vars
				</cfscript>
			<cfelse>
				<cfscript>
					thisSession = CreateObject("component", "_cfcs.SessionMgmt");
					RenewSession = thisSession.extnSession(Session_UID=SESSION.SESSION_UID);
				</cfscript>
			</cfif>
		</cfif>
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	
	<cffunction
		name="OnRequest"
		access="public"
		returntype="void"
		output="true"
		hint="Processes the requested template.">
		 
		<!--- Define arguments. --->
		<cfargument
			name="TargetPage"
			type="string"
			required="true"
			hint="The template being requested." default="#CGI.SCRIPT_NAME#"
			/>
		<cfset requestPage = ReReplaceNoCase(ARGUMENTS.TargetPage, "^/", "./")>
		
		<!--- Include the requested template. --->
		<cfinclude template="#requestPage#" />
		 
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
	
	<cffunction name="onError">
    <!--- The onError method gets two arguments:
			An exception structure, which is identical to a cfcatch variable.
			The name of the Application.cfc method, if any, in which the error
			happened.--->
		<cfargument name="Except" required="yes" />
		<cfargument type="String" name="EventName" required="yes" />
		<!--- Log all errors in an application-specific log file. --->
		<cflog file="#This.Name#" type="error" text="Event Name: #Eventname#" >
		<cflog file="#This.Name#" type="error" text="Message: #except.message#">
		<!--- Throw validation errors to ColdFusion for handling. --->
		<cfif Find("coldfusion.filter.FormValidationException", Arguments.Except.StackTrace)>
			<cfthrow object="#except#">
		<cfelse>
			<cfscript>
				ERR = StructNew();
				ERR.Error = except;
			</cfscript>
			<cfinclude template="./security/_tmp_error.cfm">
		</cfif>
	</cffunction>
	
	 <!---<cffunction
		name="OnSessionStart"
		access="public"
		output="true"
		hint="Fires when the session is first created.">

		<cfscript>
			thisSession = CreateObject("component", "_cfcs.SessionMgmt");
			CreateSession = thisSession.initSession();
		</cfscript>

		Return out. 
		<cfreturn />
	</cffunction>--->

	<cffunction
		name="OnApplicationEnd"
		access="public"
		returntype="void"
		output="false"
		hint="Fires when the application is terminated.">
		
		<!--- Define arguments. --->
		<cfargument
			name="ApplicationScope"
			type="struct"
			required="false"
			default="#StructNew()#"/>
		
		<!--- Return out. --->
		<cfreturn />
	</cffunction>
</cfcomponent>