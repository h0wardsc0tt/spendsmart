<cfsilent>
<cfparam name="URL.o" default="0">
<cfparam name="URL.lm" default="10">

<cfif (IsDefined("URL.pdt") AND ReFindNoCase("[a-z0-9]{32}", URL.pdt)) AND (IsDefined("URL.st") AND URL.st IS "add")>
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getProds_Wish";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.OrderBy = "prod.Product_Short_Description";
		
		QUERY.Product_UID = URL.pdt;
		QUERY.SelectSQL = "prod.*, type.ProductType AS thisCategory";
	</cfscript>
	<cfinclude template="./_qry_select_cat_type_product.cfm">

	<cfif qry_getProds_Wish.RecordCount NEQ 0>
		<cfscript>
			trackProduct = CreateObject("component", "_cfcs.SessionMgmt");
			thisProduct = trackProduct.TrackView(Product_UID=qry_getProds_Wish.Product_UID,Session_UID=SESSION.Session_UID,User_Cookie=qry_getUser.User_Cookie,Wishlist=1);
		</cfscript>
	</cfif>
</cfif>

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getProducts";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.OrderBy = "prod.Product_Short_Description";
	QUERY.SelectSQL = "DISTINCT prod.Product_UID, prod.Product_Image_Thum, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.Contract_Price, prod.UOM, prod.Product_IsActive";
	QUERY.User_Cookie = qry_getUser.User_Cookie;
	QUERY.History_Wishlist = 1;
</cfscript>
<cfinclude template="./_qry_select_history.cfm">

<cfscript>
	Attribs = StructNew();
	Attribs.Str_Row = URL.o;
	Attribs.Tot_Row = URL.lm;
	Attribs.Totals = qry_getProducts_Total.Records;
	
	recStr = Evaluate(URL.o+1);
	recEnd = Evaluate(URL.lm+URL.o);
	if(recEnd GT qry_getProducts.RecordCount) {
		recEnd = qry_getProducts.RecordCount;
	}
	PageIsWishlist = true;
</cfscript>
</cfsilent>

<cfoutput>
<div id="List" class="products clear">
	<h2>User Wishlist</h2>
	<cfinclude template="./_tmp_product_list.cfm">
</div>
</cfoutput>