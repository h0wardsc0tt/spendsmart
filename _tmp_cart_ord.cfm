<cfsilent>
	<!---Validate Session and insert/create invoice--->
	<cfparam name="URL.suid" default="">
	<cfparam name="FORM.suid" default="">
	<cfparam name="get_Session" default="">
	
	<cfscript>
		if(ReFindNoCase("[a-z0-9]{32}", URL.suid) NEQ 0) { //Check URL
			get_Session = URL.suid;
		}
		if(ListLen(FORM.suid) GT 0 AND ReFindNoCase("[a-z0-9]{32}", FORM.suid) NEQ 0) { //Check Form
			get_Session = FORM.suid; 
		} 
		if(Len(get_Session) EQ 0) { //Assume no URL or FORM
			get_Session = SESSION.Session_UID;
			/*manageCart = CreateObject("component", "_cfcs.CartMgmt");
			ordCart = manageCart.placeOrder(Session_UID=SESSION.Session_UID,User_Cookie=qry_getUser.User_Cookie); //Validate Product and retrieve record*/
		}
		
		/*if(FindNoCase("Thank you", ordCart) GT 0) { //Reinit session on successful order 
			CreateSession = CreateObject("component", "_cfcs.SessionMgmt");
			Session_UID = CreateSession.initSession(User_Cookie=qry_getUser.User_Cookie);
		}*/
	</cfscript>
	
		<cfparam name="cXML_Order" default="">
		<cfparam name="Status_Code" default="">
		<cfparam name="Status_Text" default="">
		<cfparam name="Order_Status" default="">
		
		<!---Submit Puchout Request--->
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getInvoice";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Session_UID = get_Session;
			QUERY.User_Cookie = qry_getUser.User_Cookie;
			QUERY.SelectSQL = "DISTINCT usrs.User_From, usrs.User_To, usrs.Browser_FormPost, usrs.Contact_Name, usrs.Contact_Email, usrs.Ship_Name, usrs.Ship_Address1, usrs.Ship_Address2, usrs.Ship_City, usrs.Ship_State, usrs.Ship_Zip, usrs.Ship_Country, invc.Invoice_Session_UID, invc.Invoice_User_Cookie, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS";
			QUERY.OrderBy = "invc.Invoice_Session_UID, invc.Invoice_Order, invc.Invoice_Created_DTS";
		</cfscript>
		
		<cfinclude template="./_qry_select_user_invoice.cfm"><!---Remember to remove . if placed back in a CFC--->
		
		<cfif qry_getInvoice.RecordCount NEQ 0>
			<cfoutput>
			<cfxml casesensitive="no" variable="cXML_Order"><?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE cXML SYSTEM "http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd">
			<cXML xml:lang="en-US" payloadID="#get_Session#" timestamp="#DateFormat(DateAdd('h', 8, Now()),'yyyy-mm-dd')#T#TimeFormat(DateAdd('h', 8, Now()), 'hh:mm:ss')#-08:00">
			<Header>
				<From>
					<Credential domain="DUNS">
						<Identity>#qry_getInvoice.User_To#</Identity>
					</Credential>
				</From>
				<To>
					<Credential domain="DUNS">
						<Identity>#qry_getInvoice.User_From#</Identity>
					</Credential>
				</To>
				<Sender>
					<Credential domain="hme.com">
						<Identity>HME_Spendsmart</Identity>
					</Credential>
					<UserAgent>HME Spendsmart Agent</UserAgent>
				</Sender>
			</Header>
			<Message>
				<Status code="200" text="OK">The shopping session completed successfully.</Status>
				<PunchOutOrderMessage>
					<BuyerCookie>#qry_getInvoice.Invoice_User_Cookie#</BuyerCookie>
					<PunchOutOrderMessageHeader operationAllowed="edit">
						<Total>
							<Money currency="USD">#NumberFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount], ".99")#</Money>
						</Total>
					</PunchOutOrderMessageHeader>
					<cfloop query="qry_getInvoice" startrow="1" endrow="#Evaluate(qry_getInvoice.RecordCount-1)#"><ItemIn quantity="#Invoice_Product_Qty#" lineNumber="#Invoice_Order#">
						<ItemID>
							<SupplierPartID>#Invoice_Manufacturer_Part_Number#</SupplierPartID>
						</ItemID>
						<ItemDetail>
							<UnitPrice>
								<Money currency="USD">#NumberFormat(Invoice_Product_Price, ".99")#</Money>
							</UnitPrice>
							<Description>
								<![CDATA[#Invoice_Product_Description#]]>
							</Description>
							<UnitOfMeasure>#Invoice_UOM#</UnitOfMeasure>
							<Classification domain="UNSPSC">#Invoice_UNSPSC#</Classification>
						</ItemDetail>
					</ItemIn></cfloop>
				</PunchOutOrderMessage>
			</Message>
			</cXML>
			</cfxml>
			</cfoutput>
			<!---Complete Cart--->
			<cfquery name="qry_FinalizeCart" datasource="#APPLICATION.Datasource#">
				UPDATE stbl_User_Cart_Products
				SET Cart_IsCompleted = 1
				WHERE 
					Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#get_Session#">
				AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qry_getInvoice.Invoice_User_Cookie#">
			</cfquery>
		</cfif>
		
		<cfset MyOrderRequest = URLEncodedFormat(cXML_Order)>
		
		<cfscript>
			//re-init session
			CreateSession = CreateObject("component", "_cfcs.SessionMgmt");
			Session_UID = CreateSession.initSession(User_Cookie=qry_getInvoice.Invoice_User_Cookie);
		</cfscript>
</cfsilent>	

<cfoutput>
<div id="List" class="products clear">
	<h2>Processing Order...</h2>
	<div class="loading"><img src="/images/loading.gif" /></div>
	<h3><form name="PunchoutOrder" action="#qry_getInvoice.Browser_FormPost#" method="post"><input type="hidden" name="oracleCart" value="#MyOrderRequest#" /></form></h3>
	<div class="clear"></div>
</div>
</cfoutput>

<script type="text/javascript">
	$(document).ready(function(e) { //On doc load submit form to Vinimaya
		document.PunchoutOrder.submit();
	});
</script>