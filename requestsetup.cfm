<!--- Get the URL. --->
<cfset strURL = (CGI.server_name & GetDirectoryFromPath(CGI.script_name) & "requestauth.cfm") />
 
<!--- Create the XML data to post. --->
<cfsavecontent variable="strXML">
<?xml version="1.0"?><!DOCTYPE cXML SYSTEM "http://xml.cxml.org/schemas/cXML/1.1.008/cXML.dtd"><cXML payloadID="1346847418415.1171611@smartsearchdev.vinimaya.com" timestamp="2012-09-05T08:16:58" version="1.0" xml:lang="en">
    <Header>
        <From>
            <Credential domain="NetworkID">
                <Identity>macbig</Identity>
            </Credential>
        </From>
        <To>
            <Credential domain="DUNS">
                <Identity>159148746</Identity>
            </Credential>
        </To>
        <Sender>
            <Credential domain="NetworkUserId">
                <Identity>macbig</Identity>
                <SharedSecret>Fries</SharedSecret>
            </Credential>
            <UserAgent>Vinimaya Intelligent Agent</UserAgent>
        </Sender>
    </Header>
    <Request deploymentMode="production">
        <PunchOutSetupRequest operation="create">
            <BuyerCookie>-5492071800409275739</BuyerCookie>
            <BrowserFormPost>
                <URL>http://smartsearchdev.vinimaya.com/vsscmcd53/receiveorder.do?supplierId=159&amp;transactionid=-5492071800409275739</URL>
            </BrowserFormPost>
            <Contact role="endUser">
                <Name xml:lang="en">HINMAN, TERRY</Name>
                <Email>tech-support@vinimaya.com</Email>
            </Contact>
            <SupplierSetup>            <URL>https://b2bconnect.supplier.com:443/invoke/wm.b2b.cxml/receiveCXML</URL>
            </SupplierSetup>
            <ShipTo>
                <Address addressID="">
                    <Name xml:lang="en"/>
                    <PostalAddress>
                        <DeliverTo>195500549033 US US HOME OFFICE*800</DeliverTo>
                        <Street></Street>
						<Street></Street>
                        <City></City>
                        <State></State>
                        <PostalCode></PostalCode>
                        <Country isoCountryCode="USA">United States</Country>
                    </PostalAddress>
                </Address>
            </ShipTo>        
	</PunchOutSetupRequest>
    </Request>
</cXML>
</cfsavecontent>

<!---
	Post the XML data to catch page. We are going
	to post this value as an XML object will actually
	just post it as an XML body.
--->
<cfhttp
	url="#strURL#"
	method="POST"
	useragent="#CGI.http_user_agent#"
	result="objGet">
	<!---
		When posting the xml data, remember to trim
		the value so that it is valid XML.
	--->
	<cfhttpparam
		type="XML"
		value="#strXML.Trim()#"
		/>
</cfhttp>
 
<!--- Output the return message. --->
<cfscript>
	res_XML = objGet.FileContent.Trim();
	if(IsXML(res_XML)) {
		dat_XML = XMLParse(res_XML);
		get_Status = XmlSearch(dat_XML, "/cXML/Response/Status");
		Status = get_Status[1].XmlAttributes;
		Status_Code = Status.code;
		Status_Text = Status.text;
		
		get_URL = XmlSearch(dat_XML, "/cXML/Response/PunchOutSetupResponse/StartPage/URL");
		red_URL = get_URL[1].XmlText;
	}
</cfscript>

<cfdump var="#objGet.FileContent#">
<cfif IsDefined("Status_Code") AND Status_Code EQ 200>
	<cflocation url="#red_URL#" addtoken="no">
	
</cfif>
<cfabort>