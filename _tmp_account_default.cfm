<cfsilent>
	<cfparam name="URL.o" default="0">
	<cfparam name="URL.lm" default="10">

	<cfscript>	
		QUERY = StructNew();
		QUERY.QueryName = "qry_getInvoice";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.User_Cookie = qry_getUser.User_Cookie;
		QUERY.SelectSQL = "DISTINCT usrs.User_From, usrs.User_To, usrs.Contact_Name, usrs.Contact_Email, usrs.Ship_Name, usrs.Ship_Address1, usrs.Ship_Address2, usrs.Ship_City, usrs.Ship_State, usrs.Ship_Zip, usrs.Ship_Country, invc.Invoice_Session_UID, invc.Invoice_User_Cookie, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS, DatePart(month, invc.Invoice_Created_DTS) AS myMonth, DatePart(day, invc.Invoice_Created_DTS) AS myDay, DatePart(year, invc.Invoice_Created_DTS) AS myYear";
		QUERY.GroupBy = "invc.Invoice_Session_UID, invc.Invoice_User_Cookie, usrs.User_From, usrs.User_To, usrs.Contact_Name, usrs.Contact_Email, usrs.Ship_Name, usrs.Ship_Address1, usrs.Ship_Address2, usrs.Ship_City, usrs.Ship_State, usrs.Ship_Zip, usrs.Ship_Country, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS";
		QUERY.Cart_IsCompleted = 1;
		QUERY.OrderBy = "myYear DESC, myMonth DESC, myDay DESC, invc.Invoice_Session_UID, invc.Invoice_Order";
	</cfscript>
	<cfinclude template="./_qry_select_user_invoice.cfm">
	
	<cfscript>
		Attribs = StructNew();
		Attribs.Str_Row = URL.o;
		Attribs.Tot_Row = URL.lm;
		Attribs.Totals = qry_getInvoice_Total.Records;
		
		recStr = Evaluate(URL.o+1);
		recEnd = Evaluate(URL.lm+URL.o);
		if(recEnd GT qry_getInvoice.RecordCount) {
			recEnd = qry_getInvoice.RecordCount;
		}
	</cfscript>
</cfsilent>	

<div id="List" class="products clear">
	<!---<h2>Account Information</h2><cfif qry_getInvoice.RecordCount NEQ 0>
	<table class="shipto"><cfoutput>
		<tr>
			<td>#qry_getInvoice.Ship_Name#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_Address1#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_Address2#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_City#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_State#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_Zip#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_Country#</td>
		</tr></cfoutput>
	</table>--->
	<h2>Past Items Ordered</h2><cfif qry_getInvoice.RecordCount NEQ 0>
	<table>
		<cfif ERR.ErrorFound>
		<tr>
			<td><cfoutput>
			<ul>
				<cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li></cfloop>
			</ul>
			</cfoutput></td>
		</tr>
		</cfif><cfset y = 1>
		<cfoutput query="qry_getInvoice" group="Invoice_Session_UID">
		<tr>
			<th colspan="4" class="ord_date">Order Date: #DateFormat(Invoice_Created_DTS, "MMMM DD, YYYY")#</th>
			<th><a href="./?pg=Cart&st=ReOrder&isu=#Invoice_Session_UID#">Reorder</a></th>
		</tr>
		<tr>
			<th>Quantity</th>
			<th>Product Name</th>
			<th>Manufacturer Part Number</th>
			<th>UOM</th>
			<th>Price</th>
		</tr><cfset x = 1>
		<cfoutput><cfif FindNoCase("Sub-Total", Invoice_Product_Description) EQ 0 AND Len(Invoice_Product_UID) EQ 32>
		<tr class="#IIF(x MOD 2, DE('noshade'), DE('shade'))#">
			<td>#Invoice_Product_QTY#</td>
			<td>#Invoice_Product_Description#</td>
			<td>#Invoice_Manufacturer_Part_Number#</td>
			<td>#Invoice_UOM#</td>
			<td>#DollarFormat(Invoice_Product_Price)#</td>
		</tr><cfelse>
		<tr>
			<td colspan="5"><div class="subtotal">Subtotal:&nbsp;<span id="subprice">#DollarFormat(Invoice_Product_Price)#</span></div></td>
		</tr></cfif><cfset x = x+1></cfoutput><cfset y = y+1></cfoutput>
		<tr>
			<td colspan="5"><cfmodule template="./_mod_next_prev.cfm" OFFSET="#URL.o#" LIMIT="#URL.lm#" RECORDCOUNT="#Evaluate((URL.lm+URL.o))#" TOTALCOUNT="#y#" POST="false"></td>
		</tr>
	</table><cfelse>
	<table>
		<tr>
			<td><h3>Order history not found.</h3></td>
		</tr>
	</table></cfif>
	<div class="clear"></div>
</div>