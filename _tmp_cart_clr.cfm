<cfsilent>
	<cfparam name="ERR" default="#StructNew()#">
	<cfparam name="ERR.ErrorFound" default="false">
	<cfparam name="ERR.ErrorMessage" default="">
	
	<!---Validation and insertion--->
	<cfscript>
		if(NOT ERR.ErrorFound) {
			manageCart = CreateObject("component", "_cfcs.CartMgmt");
			clearProducts = manageCart.clearCart(Session_UID=SESSION.Session_UID,User_Cookie=qry_getUser.User_Cookie); 
		}
	</cfscript>
</cfsilent>

<cfinclude template="./_tmp_cart_default.cfm">