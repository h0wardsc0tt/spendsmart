<cfsilent>
<cfparam name="PageIsWishlist" default="false">
<cfscript>
	if(PageIsWishlist) {
		numCols = 8;
		noResults = "You have no items on your wishlist";
	} else {
		numCols = 7;
		noResults = "Your search returned no results";
	}
</cfscript>

<cfsavecontent variable="AddToCart">
<!---Raw Javascript Obfuscate code before making live.--->
<script type="text/javascript"> 
	<cfoutput>
	function html_CartCount(data) {
		var ret_result = "";
		$.each(data, function(index, data) {
			if(data.carttotal != "fail") {
				var cartcount = document.getElementById("cartcount");
				cartcount.innerHTML = " (" + data.carttotal + ")";
				
				var $dialog = $('<div class="itemadded"></div>')
					.html('The Product Has Been Successfully Added To Your Shopping Cart.<div class="shopchoice"><span><a href="javascript:void(false);" onClick="return closeAdd();">Continue Shopping</a></span><span><a href="./?pg=Cart">View Cart (' + data.carttotal + ')</a></span></div>')
					.dialog({
						autoOpen: false,
						title: 'Item Added'
					});
				$dialog.dialog("open");

				ret_result = "pass";
			} else {
				ret_result = "fail";
			}
		});
		
		return ret_result;
	}
	function closeAdd() {
		$(".itemadded").dialog('close');
		return false;
	}
	function addCart(field_id) {
		var jadd_act = "/js/addCart.cfm";
		$.ajax({
			url: jadd_act, 
			data: ({ sess: '#SESSION.Session_UID#', pdt: field_id, qty: 1, uco: '#qry_getUser.User_Cookie#' }),
			dataType: "json",
			success: function(data) {
				var set_msg = html_CartCount(data);
				if(set_msg == "fail") {
					alert("Error occured");
					window.location.href = "https://#CGI.HTTP_HOST#";
				}
			}
		});
	}
	</cfoutput>
</script>
</cfsavecontent>
<cfhtmlhead text="#AddToCart#">

</cfsilent>
<cfoutput>
<table>
	<tr>
		<th class="thm"></th>
		<th>Product Name</th>
		<th>Supplier Part Number</th>
		<th>Manufacturer Part Number</th>
		<th>UOM</th>
		<th>Contract Price</th>
		<th></th>
		<cfif PageIsWishlist><th></th></cfif>
	</tr>
	<cfif qry_getProducts.RecordCount NEQ 0><cfloop query="qry_getProducts" startrow="#recStr#" endrow="#recEnd#"><cfif Product_IsActive EQ 1>
	<tr class="#IIF(CurrentRow MOD 2, DE('noshade'), DE('shade'))#">
		<td><a href="./?pg=Detail&pdt=#Product_UID#"><cfif FileExists("#APPLICATION.rootDir#\images\products\#Product_Image_Thum#")><img src="/images/products/#Product_Image_Thum#" class="p_thumb" /><cfelse><img src="/images/products/noimage.png" class="p_thumb" /></cfif></a></td>
		<td><a href="./?pg=Detail&pdt=#Product_UID#">#Product_Short_Description#</a></td>
		<td>#Supplier_Part_Number#</td>
		<td>#Manufacturer_Part_Number#</td>
		<td>#UOM#</td>
		<td>#DollarFormat(Contract_Price)#</td>
		<td><a href="javascript:void(false);" onclick="return addCart('#Product_UID#');"><img src="/images/addcart_but.png" /></a></td>
		<cfif PageIsWishlist><td><a href="./?pg=Wishlist&st=Del&pdt=#Product_UID#">Delete</a></td></cfif>
	</tr></cfif></cfloop>
	<tr>
		<td colspan="#numCols#">
			<cfmodule template="./_mod_next_prev.cfm" OFFSET="#URL.o#" LIMIT="#URL.lm#" RECORDCOUNT="#Evaluate((URL.lm+URL.o))#" TOTALCOUNT="#qry_getProducts_Total.Records#" POST="true">
		</td>
	</tr><cfelse>
	<tr>
		<td colspan="#numCols#"><h3>#noResults#</h3></td>
	</tr></cfif>
</table>
</cfoutput>
<div class="clear"></div>
<cfinclude template="./_tmp_product_recommended.cfm">