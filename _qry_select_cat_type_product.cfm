<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getProducts">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.SelectSQL" default="prod.*, type.*, cats.*">
<cfparam name="QUERY.Product_IsActive" default="1">
<cfparam name="QUERY.OrderBy" default="prod.Product_Short_Description">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT #QUERY.SelectSQL#
	FROM 
		tbl_Products prod 
		INNER JOIN (ltbl_Product_Type type
		INNER JOIN (ltbl_Product_Category cats
		INNER JOIN itbl_Product_Category_Type ipct
			ON cats.ProductCategory_ID = ipct.ProductCategory_ID) 
			ON type.ProductType_ID = ipct.ProductType_ID) 
			ON prod.Product_ID = ipct.Product_ID
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "ProductCategory_UID")>
	AND cats.ProductCategory_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.ProductCategory_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductCategory_ID")>
	AND cats.ProductCategory_ID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.ProductCategory_ID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductType_UID")>
	AND type.ProductType_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.ProductType_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "ProductType_ID")>
	AND type.ProductType_ID IN (<cfqueryparam cfsqltype="cf_sql_integer" value="#QUERY.ProductType_ID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Product_UID")>
	AND prod.Product_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Product_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Product_IsActive")>
	AND prod.Product_IsActive IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.Product_IsActive#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Product_UID_NOT_IN")>
	AND prod.Product_UID NOT IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Product_UID_NOT_IN#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Product_SEARCH")>
	AND 
		(
			0=1
			OR prod.Supplier_Part_Number LIKE (<cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.Product_SEARCH#%">)
			OR prod.Manufacturer_Part_Number LIKE (<cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.Product_SEARCH#%">)
			OR prod.Product_Short_Description LIKE (<cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.Product_SEARCH#%">)
			OR type.ProductType LIKE (<cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.Product_SEARCH#%">)
			OR cats.ProductCategory LIKE (<cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.Product_SEARCH#%">)
		)
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>

<cfif StructClear(QUERY)></cfif>
