<cfsilent>
<cfparam name="GreetUser" default="">
<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getTypes";
	QUERY.Datasource = APPLICATION.Datasource;
	//QUERY.ProductType_IsActive = "1";
	QUERY.ProductCategory_IsActive = "1";
	QUERY.ProductCategory_ID_NOT_IN = 8;
	QUERY.SelectSQL = "cats.*, type.*";
	QUERY.OrderBy = "cats.ProductCategory, type.ProductType";
</cfscript>
<cfinclude template="./_qry_select_cat_type.cfm">

<cfif StructKeyExists(SESSION, "Session_UID")>
	<cfquery name="qry_getUser" datasource="#APPLICATION.Datasource#" maxrows="1">
		SELECT users.*, sess.*
		FROM 
			tbl_Users users 
			INNER JOIN dtbl_User_Session sess ON users.User_Cookie = sess.User_Cookie
		WHERE sess.User_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
	</cfquery>
	
	<cfif qry_getUser.RecordCount NEQ 0>
		<cfset GreetUser = qry_getUser.Contact_Name>
	</cfif>
</cfif>
</cfsilent>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SpendSmart Marketplace</title>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<link rel="stylesheet" type="text/css" href="/css/style.sps.css" />
<link rel="stylesheet" type="text/css" href="/css/style.ddm.css" />
<link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.8.22.custom.css" />
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script src="/js/jquery-ui-1.8.21.custom.min.js" language="JavaScript" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/style.box.css" media="screen" />
<script type="text/javascript">
	$(function() {
		$('a.lightbox').lightBox();
	});
</script>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-32497096-1']);
	_gaq.push(['_setDomainName', '.hme.com']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
</head>

<body>
<div id="Page">
	<div id="Header">
		<div class="leftLogo">
			<a href="./"><img src="/images/hme_logo.png" /></a>
		</div>
		<div class="midLogo">
			<cfif GreetUser IS NOT ""><cfoutput>
			<div class="greet">Hello, #GreetUser#</div>
			<div class="account">
				<ul>
					<li><a href="./?pg=Account">Account</a></li>
					<li class="cart"><a href="./?pg=Cart">Cart</a><span id="cartcount"></span></li>
					<li><a href="./?pg=Wishlist">Wishlist</a><span id="wishqty"></span></li>
				</ul>
			</div>
			</cfoutput></cfif>
		</div>
		<div class="rightLogo">
			<img src="/images/spendsmart_logo.png" />
		</div>
	</div>
	<div id="Navbar">
		<div id="main-nav">
			<ul id="nav" class="dropdown dropdown-horizontal">
				<li class="dir">Shop by <span class="ucase">Category</span>
					<ul>
						<cfoutput query="qry_getTypes" group="ProductCategory_ID">
						<li class="dor">
							<a href="./?pg=Product&pcu=#ProductCategory_UID#">#ProductCategory#</a>
							<cfif ProductType_IsActive EQ 1>
							<ul>
								<!---2012/09/21 ATN: Removed before official go live. Need to build systems<li class="dor"><a href="./?pg=NewHeadset">New System</a></li>
								<li class="dor"><a href="./?pg=NewHeadset">Upgrade Existing System</a></li>--->
								<li class="dor">
									System Components and Accessories
									<cfif ProductType_IsActive EQ 1>
									<ul>
										<cfoutput>
										<li class="dor"><a href="./?pg=Product&ptu=#ProductType_UID#">#ProductType#</a></li>
										</cfoutput>
									</ul>
									</cfif>
								</li>
							</ul>
							</cfif>
						</li>
						</cfoutput>
					</ul>
				</li>
			</ul>
		</div>
		<div id="searchwrapper">
		<form name="form1" method="post" action="./?pg=Search" />
			<div class="searchFrame">
			<input name="criteria" type="text" class="searchBox" id="criteria" size="8" />
			<input type="image" src="/images/search.png" class="searchBox_submit" value="" />
			</div>
		</form>
		</div>
		<div id="chat"></div>
		<div id="contact"><h4>800.848.4468</h4></div>
	</div>