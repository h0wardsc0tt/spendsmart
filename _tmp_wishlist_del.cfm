<cfsilent>
	<cfparam name="URL.pdt" default="">
	<cfparam name="ERR" default="#StructNew()#">
	<cfparam name="ERR.ErrorFound" default="false">
	<cfparam name="ERR.ErrorMessage" default="">
	
	<!---Validation and insertion--->
	<cfscript>
		if(ReFindNoCase("[a-z0-9]{32}", URL.pdt) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Sorry, the product you are trying to remove does not exist");
		}
		if(NOT ERR.ErrorFound) {
			manageCart = CreateObject("component", "_cfcs.CartMgmt");
			valProduct = manageCart.valItem(Product_UID=URL.pdt); //Validate Product and retrieve record
			
			if(valProduct.Tot_Records NEQ 0) { //Meaning that this an active and valid product
				manageSession = CreateObject("component", "_cfcs.SessionMgmt");
				delWish = manageSession.remWish(User_Cookie=qry_getUser.User_Cookie,Product_UID=URL.pdt);
			} else {
				ERR.ErrorFound = true;
				ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Sorry, the product you are trying to remove does not exist");
			}
		}
	</cfscript>
</cfsilent>

<cfinclude template="./_tmp_product_wishlist.cfm">