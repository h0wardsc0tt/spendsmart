<cfcomponent displayname="ShoppingCart" hint="Controls the user's shopping cart">
	<cfparam name="APPLICATION.Datasource" default="#APPLICATION.Datasource#">
	
<!---Validate the product as both existing as well as active --->
	<cffunction name="valItem" output="no" returntype="struct" hint="Validates an item">
		<cfargument name="Product_UID" required="yes">
		<cfargument name="Product_IsActive" default="1" required="no">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_validateProduct";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Product_UID = Product_UID;
			QUERY.Product_IsActive = Product_IsActive;
		</cfscript>
		<cfinclude template="/_qry_select_products.cfm">
		
		<cfscript>
			Ret_Records = StructNew();
			Ret_Records.Tot_Records = qry_validateProduct.RecordCount;
			Ret_Records.Dat_Records = qry_validateProduct;
		</cfscript>
		
		<cfreturn Ret_Records>
	</cffunction>
<!---Add an item to the shopping cart--->
	<cffunction name="addItem" output="no" returntype="void" hint="Adds an item to the shopping cart">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfargument name="Product_UID" required="yes">
		<cfargument name="Product_QTY" default="0" required="yes">
		<cfargument name="Product_Price" default="0" required="yes">
		
		<!---Need to write routine to accomodate discounts--->
		
		<cfscript>
			insResult = "";
			
			QUERY = StructNew();
			QUERY.QueryName = "qry_checkCart";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Cart_Product_UID = Product_UID;
			QUERY.Cart_Session_UID = Session_UID;
			QUERY.Cart_User_Cookie = User_Cookie;
			QUERY.OrderBy = "cart.Cart_DTS";
		</cfscript>
		<cfinclude template="/_qry_select_cart_product.cfm">
		
		<!--- Only insert new items into cart db--->
		<cfif qry_checkCart.RecordCount EQ 0>
			<cftry>
				<cfquery name="qry_insItem" datasource="#APPLICATION.Datasource#" maxrows="1">
					INSERT INTO stbl_User_Cart_Products
						(Cart_Session_UID,
						Cart_User_Cookie,
						Cart_Product_UID,
						Cart_Product_QTY,
						Cart_Product_Price_SubTotal,
						Cart_DTS)
					VALUES
						(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#Product_QTY#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#Product_Price#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">)
				</cfquery>
				<cfcatch type="any"><!---Need to write onError event---></cfcatch>
			</cftry>
		</cfif>
	</cffunction>
<!---Update Quantities--->
	<cffunction name="qtyItem" output="no" returntype="void" hint="Changes product qty">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfargument name="Product_UID" required="yes">
		<cfargument name="Product_QTY" default="0" required="yes">
		
		<cfquery name="qry_qtyItem" datasource="#APPLICATION.Datasource#" maxrows="1">
			UPDATE stbl_User_Cart_Products
			SET 
				Cart_Product_QTY = <cfqueryparam cfsqltype="cf_sql_integer" value="#Product_QTY#">
			WHERE 0=0
			AND	Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
			AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
			AND Cart_Product_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">
		</cfquery>
	</cffunction>
<!---Remove an item from the shopping cart--->
	<cffunction name="delItem" output="no" returntype="void" hint="Deletes an item to the shopping cart">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfargument name="Product_UID" required="yes">
		
		<cfquery name="qry_delItem" datasource="#APPLICATION.Datasource#" maxrows="1">
			DELETE
			FROM stbl_User_Cart_Products
			WHERE 0=0
			AND	Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
			AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
			AND Cart_Product_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">
		</cfquery>
	</cffunction>
<!----Clear all items currently in shopping cart--->	
	<cffunction name="clearCart" output="no" returntype="void" hint="Clears the shopping cart">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		
		<cfquery name="qry_delItem" datasource="#APPLICATION.Datasource#">
			DELETE
			FROM stbl_User_Cart_Products
			WHERE 0=0
			AND	Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
			AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
		</cfquery>
	</cffunction>
<!---Checkout items in cart and create invoice--->
	<cffunction name="invSess" output="no" returntype="void" hint="Creates the invoice prior to checkout">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getCart";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Cart_Session_UID = Session_UID;
			QUERY.Cart_User_Cookie = User_Cookie;
			QUERY.Cart_IsCompleted = 0;
			QUERY.OrderBy = "cart.Cart_DTS";
		</cfscript>
		<cfinclude template="/_qry_select_cart_product.cfm">
		
		<cfif qry_getCart.RecordCount NEQ 0><!---Make sure user didn't manage to submit an empty cart--->
			<cfquery name="qry_clearInvoice" datasource="#APPLICATION.Datasource#"><!---Clear Invoice Table On Each Request--->
				DELETE
				FROM stbl_User_Invoice
				WHERE 0=0
				AND Invoice_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
				AND Invoice_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
			</cfquery>
			
			<cfset itemTotal = 0>
			<cfset cartTotal = 0>
			
			<cfoutput query="qry_getCart">
				<cfset itemTotal = Cart_Product_Qty * Cart_Product_Price_SubTotal>
				<cfset cartTotal = cartTotal + itemTotal>
				
				<cfquery name="qry_insInvoice" datasource="#APPLICATION.Datasource#">
					INSERT INTO stbl_User_Invoice
						(Invoice_Session_UID,
						Invoice_User_Cookie,
						Invoice_Product_UID,
						Invoice_Manufacturer_Part_Number,
						Invoice_Product_Description,
						Invoice_Product_Price,
						Invoice_Product_Discount,
						Invoice_Product_Qty,
						Invoice_UOM,
						Invoice_UNSPSC,
						Invoice_Order,
						Invoice_Created_DTS,
						Invoice_Created_IP)
					VALUES
						(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_UID#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Manufacturer_Part_Number#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#Product_Short_Description#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="#itemTotal#">,
						<cfqueryparam cfsqltype="cf_sql_money" value="0">, <!---Need to create routine for discounts--->
						<cfqueryparam cfsqltype="cf_sql_integer" value="#Cart_Product_Qty#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#UOM#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#UNSPSC#">,
						<cfqueryparam cfsqltype="cf_sql_integer" value="#CurrentRow#">,
						<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
						<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">)
				</cfquery>
			</cfoutput>
			
			<cfset totalRow = qry_getCart.RecordCount + 1>
			
			<cfquery name="qry_insInvoice" datasource="#APPLICATION.Datasource#">
				INSERT INTO stbl_User_Invoice
					(Invoice_Session_UID,
					Invoice_User_Cookie,
					Invoice_Product_Description,
					Invoice_Product_Price,
					Invoice_Product_Discount,
					Invoice_Order,
					Invoice_Created_DTS,
					Invoice_Created_IP)
				VALUES
					(<cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="Sub-Total">,
					<cfqueryparam cfsqltype="cf_sql_money" value="#cartTotal#">,
					<cfqueryparam cfsqltype="cf_sql_money" value="0">, <!---Need to create routine for discounts--->
					<cfqueryparam cfsqltype="cf_sql_integer" value="#totalRow#">,
					<cfqueryparam cfsqltype="cf_sql_timestamp" value="#CreateODBCDateTime(Now())#">,
					<cfqueryparam cfsqltype="cf_sql_varchar" value="#CGI.REMOTE_ADDR#">)
			</cfquery>
		</cfif>
	</cffunction>
	<cffunction name="placeOrder" output="no" returntype="any" hint="Creates the invoice prior to checkout">
		<cfargument name="Session_UID" required="yes">
		<cfargument name="User_Cookie" required="yes">
		<cfparam name="cXML_Order" default="">
		<cfparam name="Status_Code" default="">
		<cfparam name="Status_Text" default="">
		<cfparam name="Order_Status" default="">
		
		<!---Submit Puchout Request--->
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getInvoice";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Session_UID = Session_UID;
			QUERY.User_Cookie = User_Cookie;
			QUERY.SelectSQL = "DISTINCT usrs.User_From, usrs.User_To, usrs.Browser_FormPost, usrs.Contact_Name, usrs.Contact_Email, usrs.Ship_Name, usrs.Ship_Address1, usrs.Ship_Address2, usrs.Ship_City, usrs.Ship_State, usrs.Ship_Zip, usrs.Ship_Country, invc.Invoice_Session_UID, invc.Invoice_User_Cookie, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS";
			QUERY.OrderBy = "invc.Invoice_Session_UID, invc.Invoice_Order, invc.Invoice_Created_DTS";
		</cfscript>
		
		<cfinclude template="/_qry_select_user_invoice.cfm">
		
		<cfif qry_getInvoice.RecordCount NEQ 0>
			
			<cfoutput>
			<cfsavecontent variable="cXML_Order">
			<?xml version="1.0" encoding="UTF-8"?>
			<!DOCTYPE cXML SYSTEM "http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd">
			<cXML xml:lang="en-US" payloadID="#Session_UID#" timestamp="#DateFormat(DateAdd('h', 8, Now()),'yyyy-mm-dd')#T#TimeFormat(DateAdd('h', 8, Now()), 'hh:mm:ss')#-08:00">
			<Header>
				<From>
					<Credential domain="DUNS">
						<Identity>#qry_getInvoice.User_To#</Identity>
					</Credential>
				</From>
				<To>
					<Credential domain="DUNS">
						<Identity>#qry_getInvoice.User_From#</Identity>
					</Credential>
				</To>
				<Sender>
					<Credential domain="hme.com">
						<Identity>HME_Spendsmart</Identity>
					</Credential>
					<UserAgent>HME Spendsmart Agent</UserAgent>
				</Sender>
			</Header>
			<Message>
				<Status code="200" text="OK">The shopping session completed successfully.</Status>
				<PunchOutOrderMessage>
					<BuyerCookie>#User_Cookie#</BuyerCookie>
					<PunchOutOrderMessageHeader operationAllowed="edit">
						<Total>
							<Money currency="USD">#NumberFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount], ".99")#</Money>
						</Total>
					</PunchOutOrderMessageHeader>
					<cfloop query="qry_getInvoice" startrow="1" endrow="#Evaluate(qry_getInvoice.RecordCount-1)#"><ItemIn quantity="#Invoice_Product_Qty#" lineNumber="#Invoice_Order#">
						<ItemID>
							<SupplierPartID>#Invoice_Manufacturer_Part_Number#</SupplierPartID>
						</ItemID>
						<ItemDetail>
							<UnitPrice>
								<Money currency="USD">#NumberFormat(Invoice_Product_Price, ".99")#</Money>
							</UnitPrice>
							<Description xml:lang="en">
								<ShortName>#Invoice_Product_Description#</ShortName>
							</Description>
							<UnitOfMeasure>#Invoice_UOM#</UnitOfMeasure>
							<Classification domain="UNSPSC">#Invoice_UNSPSC#</Classification>
						</ItemDetail>
					</ItemIn></cfloop>
				</PunchOutOrderMessage>
			</Message>
			</cXML>
			</cfsavecontent>
			
			<cfhttp
				url="#qry_getInvoice.Browser_FormPost#"
				method="POST"
				useragent="#CGI.http_user_agent#"
				result="objGet">
				<!---
					When posting the xml data, remember to trim
					the value so that it is valid XML.
				--->
				<cfhttpparam
					type="XML"
					value="#cXML_Order.Trim()#"
					/>
			</cfhttp>
			</cfoutput>
			
			<cftry>
			<cfscript>
				res_XML = objGet.FileContent.Trim();
				if(IsXML(res_XML)) {
					dat_XML = XMLParse(res_XML);
					get_Status = XmlSearch(dat_XML, "/cXML/Response/Status");
					Status = get_Status[1].XmlAttributes;
					Status_Code = Status.code;
					Status_Text = Status.text;
				}
			</cfscript>
			<cfcatch type="any">
				<cfset Status_Code = 300>
				<cfset Order_Status = objGet.FileContent>
			</cfcatch>	
			</cftry>
		</cfif>
		
		<cfif Status_Code EQ 200><!---Need to change this var back to EQ before going live--->
			<cfset Order_Status = "Thank you. Your purchase order has been successfully placed.">

			<cfquery name="qry_FinalizeCart" datasource="#APPLICATION.Datasource#">
				UPDATE stbl_User_Cart_Products
				SET Cart_IsCompleted = 1
				WHERE 
					Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#Session_UID#">
				AND Cart_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#User_Cookie#">
			</cfquery>
		<cfelse>
			<cfset Order_Status = objGet.FileContent>
		</cfif>
		
		<cfreturn Order_Status>
	</cffunction>
</cfcomponent>