<cfsilent>
	<!---Validate Session and insert/create invoice--->
	<cfparam name="URL.suid" default="">
	<cfparam name="FORM.suid" default="">
	<cfparam name="get_Session" default="">
	
	<cfscript>
		if(ReFindNoCase("[a-z0-9]{32}", URL.suid) NEQ 0) { //Check URL
			get_Session = URL.suid;
		}
		if(ListLen(FORM.suid) GT 0 AND ReFindNoCase("[a-z0-9]{32}", FORM.suid) NEQ 0) { //Check Form
			get_Session = FORM.suid; 
		} 
		if(Len(get_Session) EQ 0) { //Assume no URL or FORM
			get_Session = SESSION.Session_UID;
			manageCart = CreateObject("component", "_cfcs.CartMgmt");
			invCart = manageCart.invSess(Session_UID=SESSION.Session_UID,User_Cookie=qry_getUser.User_Cookie); //Validate Product and retrieve record
		}
	</cfscript>
	
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getInvoice";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.Session_UID = get_Session;
		QUERY.SelectSQL = "DISTINCT usrs.User_From, usrs.User_To, usrs.Contact_Name, usrs.Contact_Email, usrs.Ship_Name, usrs.Ship_Address1, usrs.Ship_Address2, usrs.Ship_City, usrs.Ship_State, usrs.Ship_Zip, usrs.Ship_Country, invc.Invoice_Session_UID, invc.Invoice_User_Cookie, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS";
		QUERY.OrderBy = "invc.Invoice_Session_UID, invc.Invoice_Order, invc.Invoice_Created_DTS";
	</cfscript>
	
	<cfinclude template="./_qry_select_user_invoice.cfm">
</cfsilent>	

<cfoutput>
<div id="List" class="products clear">
	<h2>Confirm Order</h2>
	<cfif qry_getInvoice.RecordCount NEQ 0>
	<table>
		<tr>
			<th colspan="6">Items Ordered</th>
		</tr>
		<tr>
			<th>Quantity</th>
			<th>Product Name</th>
			<th>Manufacturer Part Number</th>
			<th>UOM</th>
			<th>Contract Price</th>
			<th>Total</th>
		</tr>
		<cfloop query="qry_getInvoice" startrow="1" endrow="#Evaluate(qry_getInvoice.RecordCount-1)#">
		<tr class="#IIF(CurrentRow MOD 2, DE('noshade'), DE('shade'))#">
			<td>#Invoice_Product_QTY#</td>
			<td>#Invoice_Product_Description#</td>
			<td>#Invoice_Manufacturer_Part_Number#</td>
			<td>#Invoice_UOM#</td>
			<td>#DollarFormat(Evaluate(Invoice_Product_Price/Invoice_Product_QTY))#</td>
			<td>#DollarFormat(Invoice_Product_Price)#</td>
		</tr></cfloop>
		<tr>
			<td colspan="6"><div class="subtotal">Subtotal:&nbsp;<span id="subprice">#DollarFormat(qry_getInvoice.Invoice_Product_Price[qry_getInvoice.RecordCount])#</span></div></td>
		</tr>
		<tr>
			<td colspan="3"><a href="./?pg=Search"><img src="/images/conshop_but.png" /></a></td>
			<td colspan="3" class="checkout"><form action="./?pg=Cart&st=Order" name="frm_cart" method="post" class="sscart"><input type="image" src="/images/cart_submit.png" /></form></td>
		</tr>
	</table><cfsilent>
<!---	
	<table class="shipto">
		<tr>
			<th>Customer Information</th>
		</tr>
		<tr>
			<td>#qry_getInvoice.Ship_Name#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_Address1#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_Address2#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_City#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_State#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_Zip#</td>
		</tr> 
		<tr>
			<td>#qry_getInvoice.Ship_Country#</td>
		</tr> 
		<tr>
			<td class="checkout"><form action="./?pg=Cart&st=Order" name="frm_cart" method="post" class="sscart"><input type="image" src="/images/cart_submit.png" /></form></td>
		</tr>
	</table>---></cfsilent><cfelse>
	<table>
		<tr>
			<td><h3>Invoice Not Found</h3></td>
		</tr>
	</table></cfif>
	<div class="clear"></div>
</div>
</cfoutput>

<!---<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_product_detail.cfm">
	<cfexit method="exittemplate">
<cfelse>
	<cfinclude template="./_tmp_cart_invoice.cfm">
</cfif>--->