<cfsilent>
	<cfif IsDefined("qry_getUser") AND IsQuery(qry_getUser)>
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getCart";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Cart_Session_UID = SESSION.Session_UID;
			QUERY.Cart_User_Cookie = qry_getUser.User_Cookie;
			QUERY.Cart_IsCompleted = 0;
			QUERY.OrderBy = "cart.Cart_DTS";
		</cfscript>
		<cfinclude template="/_qry_select_cart_product.cfm">
		
		<cfif qry_getCart.RecordCount NEQ 0>
			<cfsavecontent variable="itemcount">
				<script type="text/javascript">
					$(document).ready(function(e) {
						var cartcount = document.getElementById("cartcount");
						cartcount.innerHTML = <cfoutput>" (#qry_getCart.RecordCount#)"</cfoutput>;
					});
				</script>
			</cfsavecontent>
			<cfhtmlhead text="#itemcount#">
		</cfif>
	</cfif>
</cfsilent>
	<div id="Footer">
		<div class="copyright">
			&copy;<cfoutput>#Year(Now())#</cfoutput> HM Electronics, Inc. All Rights Reserved.
		</div>
		<div class="footerLogo clear">
			<!---2012/09/05 ATN: Removed per McD Legal request<img src="/images/spendsmart_logo_sm.png" />--->
		</div>
	</div>
</div>
</body>
</html>
<br clear="all" />