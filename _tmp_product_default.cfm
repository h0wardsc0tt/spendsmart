<cfsilent>
<cfparam name="URL.o" default="0">
<cfparam name="URL.lm" default="10">

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getProducts";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.OrderBy = "prod.Product_Short_Description";
	
	if(IsDefined("URL.pcu") AND ReFindNoCase("[a-z0-9]{32}", URL.pcu)) {
		QUERY.ProductCategory_UID = URL.pcu;
		QUERY.SelectSQL = "DISTINCT prod.Product_UID, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.UOM, prod.Product_Image_Thum, prod.Contract_Price, prod.Product_IsActive, cats.ProductCategory AS thisCategory";
	}
	if(IsDefined("URL.ptu") AND ReFindNoCase("[a-z0-9]{32}", URL.ptu)) {
		QUERY.ProductType_UID = URL.ptu;
		QUERY.SelectSQL = "DISTINCT prod.Product_UID, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.UOM, prod.Product_Image_Thum, prod.Contract_Price, prod.Product_IsActive, type.ProductType AS thisCategory";
	}
	if(IsDefined("URL.pdt") AND ReFindNoCase("[a-z0-9]{32}", URL.pdt)) {
		QUERY.Product_UID = URL.pdt;
		QUERY.SelectSQL = "DISTINCT prod.Product_UID, prod.Product_Short_Description, prod.Supplier_Part_Number, prod.Manufacturer_Part_Number, prod.UOM, prod.Product_Image_Thum, prod.Contract_Price, prod.Product_IsActive, type.ProductType AS thisCategory";
	}
</cfscript>
<cfinclude template="./_qry_select_cat_type_product.cfm">

<cfscript>
	Attribs = StructNew();
	Attribs.Str_Row = URL.o;
	Attribs.Tot_Row = URL.lm;
	Attribs.Totals = qry_getProducts_Total.Records;
	
	recStr = Evaluate(URL.o+1);
	recEnd = Evaluate(URL.lm+URL.o);
	if(recEnd GT qry_getProducts.RecordCount) {
		recEnd = qry_getProducts.RecordCount;
	}
</cfscript>

</cfsilent>
<cfoutput>
<div id="List" class="products clear">
	<h2>#qry_getProducts.thisCategory#</h2>
	<cfinclude template="./_tmp_product_list.cfm">
</div>
</cfoutput>