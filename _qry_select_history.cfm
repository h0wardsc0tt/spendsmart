<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getTypes">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.SelectSQL" default="prod.*, hist.*">
<cfparam name="QUERY.OrderBy" default="prod.Product_Short_Description">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT #QUERY.SelectSQL#
	FROM 
		stbl_User_History hist
		INNER JOIN tbl_Products prod ON hist.History_Product_UID = prod.Product_UID
		INNER JOIN itbl_Product_Category_Type type ON prod.Product_ID = type.Product_ID
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "User_Cookie")>
	AND hist.History_User_Cookie = <cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.User_Cookie#">
	</cfif>
	<cfif StructKeyExists(QUERY, "History_Wishlist")>
	AND hist.History_Wishlist IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.History_Wishlist#">)
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>

<cfif StructClear(QUERY)></cfif>