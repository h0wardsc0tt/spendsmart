<cfsilent>
<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getProducts";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.OrderBy = "prod.Product_Short_Description";
	
	if(IsDefined("URL.pdt") AND ReFindNoCase("[a-z0-9]{32}", URL.pdt)) {
		QUERY.Product_UID = URL.pdt;
		QUERY.SelectSQL = "prod.*, type.ProductType AS thisCategory, type.ProductType_UID";
	}
</cfscript>
<cfinclude template="./_qry_select_cat_type_product.cfm">

<cfscript>
	trackProduct = CreateObject("component", "_cfcs.SessionMgmt");
	thisProduct = trackProduct.TrackView(Product_UID=qry_getProducts.Product_UID,Session_UID=SESSION.Session_UID,User_Cookie=qry_getUser.User_Cookie);
</cfscript>

<cfscript>
	QUERY = StructNew();
	QUERY.QueryName = "qry_getRelated";
	QUERY.Datasource = APPLICATION.Datasource;
	QUERY.SelectSQL = "prod.*, type.ProductType AS thisCategory";
	QUERY.ProductType_UID = ValueList(qry_getProducts.ProductType_UID);
	QUERY.Product_UID_NOT_IN = qry_getProducts.Product_UID;
	QUERY.OrderBy = "NEWID()";
</cfscript>
<cfinclude template="./_qry_select_cat_type_product.cfm">

<cfquery name="qry_getDistinct" dbtype="query" maxrows="5">
	SELECT DISTINCT Product_UID, Product_Image_Thum, Product_Short_Description, Contract_Price
	FROM qry_getRelated
</cfquery>

</cfsilent>
<cfoutput>
<div id="Content" class="detail clear">
	<cfif IsDefined("ERR.ErrorFound") AND ERR.ErrorFound>
	<div class="error">
		<h4>The following errors occured during your request</h4>
		<ul>
			<cfloop list="#ERR.ErrorMessage#" index="thisError"><li>#thisError#</li>
			</cfloop>
		</ul>
	</div>
	</cfif>
	<div class="prod pimage">
		<cfif FileExists("#APPLICATION.rootDir#\images\products\#qry_getProducts.Product_Image_Med#")>
		<img src="/images/products/#qry_getProducts.Product_Image_Med#" /><cfelse><img src="/images/products/med_noimage.png" /></cfif>
		<cfif FileExists("#APPLICATION.rootDir#\images\products\#qry_getProducts.Product_Image_Full#")>
			<h5 class="enlarge"><a href="/images/products/#qry_getProducts.Product_Image_Full#" class="lightbox">Enlarge Image</a></h5>
		</cfif>
	</div>
	<div class="prod pdescr">
		<h2>#qry_getProducts.Product_Short_Description#</h2>
		<div class="part"><strong>Part Number:</strong> #qry_getProducts.Manufacturer_Part_Number#</div>
		<div class="descrip">#qry_getProducts.Product_Long_Description#</div>
	</div>
	<div class="prod pacart">
		<div class="cart">
			<div class="action">
				<div class="price">Price: <span>#DollarFormat(qry_getProducts.Contract_Price)#</span></div>
				<div class="stock">In Stock</div>
				<div class="addcart">
					<form action="./?pg=Cart&st=Add" method="post">
					<span>
					<input type="image" src="/images/addcart_but.png" class="addtocart" />
					<input type="hidden" name="pdt" value="#qry_getProducts.Product_UID#" />
					<select name="cqty" class="select_qty">
					<cfloop from="1" to="20" index="i"><option value="#i#">#i#</option></cfloop>
					</select>
					</span>
					</form>
				</div>
			</div>
		</div>
		<div class="wishlist">
			<h5><a href="./?pg=Wishlist&st=add&pdt=#qry_getProducts.Product_UID#">Add To Wishlist</a></h5>
		</div>
	</div>
	<div class="clear"></div>
	<div class="component clear">
		<h3>Related Components &amp; Accessories</h3>
		<cfloop query="qry_getDistinct">
		<div class="accessories">
			<a href="./?pg=Detail&pdt=#qry_getDistinct.Product_UID#">
			<cfif FileExists("#APPLICATION.rootDir#\images\products\#qry_getDistinct.Product_Image_Thum#")>
			<img src="/images/products/#qry_getDistinct.Product_Image_Thum#" /><cfelse><img src="/images/products/noimage.gif" /></cfif></a>
			<h4>#qry_getDistinct.Product_Short_Description#</h4>
			<h4>#DollarFormat(qry_getDistinct.Contract_Price)#</h4>
		</div>
		</cfloop>
	</div>
	<div class="clear"></div>
	<cfinclude template="./_tmp_product_recommended.cfm">
</div>
</cfoutput>