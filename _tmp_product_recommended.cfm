<cfsilent>
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getBrowseHistory";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.SelectSQL = "DISTINCT type.ProductType_ID";
		QUERY.History_Wishlist = 0;
		QUERY.User_Cookie = qry_getUser.User_Cookie;
		QUERY.OrderBy = "type.ProductType_ID";
	</cfscript>
	<cfinclude template="./_qry_select_history.cfm">
	
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getRecommended";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.SelectSQL = "prod.*, type.ProductType AS thisCategory";
		if(qry_getBrowseHistory.RecordCount NEQ 0) {
			QUERY.ProductType_ID = ValueList(qry_getBrowseHistory.ProductType_ID);
		}
		QUERY.OrderBy = "NEWID()";
	</cfscript>
	<cfinclude template="./_qry_select_cat_type_product.cfm">
</cfsilent>
		
<div class="component">
	<h3>Recommended Items for You</h3>
	<cfoutput query="qry_getRecommended" maxrows="5">
	<div class="accessories">
		<a href="./?pg=Detail&pdt=#Product_UID#">
		<cfif FileExists("#APPLICATION.rootDir#\images\products\#Product_Image_Thum#")>
		<img src="/images/products/#Product_Image_Thum#" /><cfelse><img src="/images/products/noimage.gif" /></cfif></a>
		<h4>#Product_Short_Description#</h4>
		<h4>#DollarFormat(Contract_Price)#</h4>
	</div>
	</cfoutput>
</div>