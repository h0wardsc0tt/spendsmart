<cfsilent>
	<cfparam name="URL.isu" default="">
	<cfparam name="FORM.pdt" default="">
	<cfparam name="FORM.cqty" default="0">
	<cfparam name="ERR" default="#StructNew()#">
	<cfparam name="ERR.ErrorFound" default="false">
	<cfparam name="ERR.ErrorMessage" default="">
	
	<!---Get Items From Invoice Table--->
	<cfscript>	
		QUERY = StructNew();
		QUERY.QueryName = "qry_getInvoice";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.User_Cookie = qry_getUser.User_Cookie;
		QUERY.Session_UID = URL.isu;
		QUERY.SelectSQL = "DISTINCT usrs.User_From, usrs.User_To, usrs.Contact_Name, usrs.Contact_Email, usrs.Ship_Name, usrs.Ship_Address1, usrs.Ship_Address2, usrs.Ship_City, usrs.Ship_State, usrs.Ship_Zip, usrs.Ship_Country, invc.Invoice_Session_UID, invc.Invoice_User_Cookie, invc.Invoice_Product_UID, invc.Invoice_Manufacturer_Part_Number, invc.Invoice_Product_Description, invc.Invoice_Product_Price, invc.Invoice_Product_Discount, invc.Invoice_Product_Qty, invc.Invoice_UOM, invc.Invoice_UNSPSC, invc.Invoice_Order, invc.Invoice_Created_DTS";
		QUERY.Cart_IsCompleted = 1;
		QUERY.OrderBy = "invc.Invoice_Order, invc.Invoice_Created_DTS DESC, invc.Invoice_Session_UID";
	</cfscript>
	<cfinclude template="./_qry_select_user_invoice.cfm">
	
	<!---Validation and insertion--->
	<cfscript>
		//Loop over Items and Insert Into Cart
		for(x=1;x LTE Evaluate(qry_getInvoice.RecordCount); x=x+1) {
			if(FindNoCase("Sub-Total", qry_getInvoice.Invoice_Product_Description[x]) EQ 0) {
				FORM.pdt = qry_getInvoice.Invoice_Product_UID[x];
				FORM.cqty = qry_getInvoice.Invoice_Product_Qty[x];
				
				if(ReFindNoCase("[a-z0-9]{32}", FORM.pdt) EQ 0) {
					ERR.ErrorFound = true;
					ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Sorry the product you are trying to add does not exist");
				}
				if(NOT IsNumeric(FORM.cqty) OR FORM.cqty LT 1) {
					ERR.ErrorFound = true;
					ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please select a valid quantity");
				}
				if(NOT ERR.ErrorFound) {
					manageCart = CreateObject("component", "_cfcs.CartMgmt");
					valProduct = manageCart.valItem(Product_UID=FORM.pdt); //Validate Product and retrieve record
					
					if(valProduct.Tot_Records NEQ 0) { //Meaning that this an active and valid product
						Product_Price = valProduct.Dat_Records.Contract_Price; //Set Contract Price
						
						addProduct = manageCart.addItem(Session_UID=SESSION.Session_UID,User_Cookie=qry_getUser.User_Cookie,Product_UID=FORM.pdt,Product_QTY=FORM.cqty,Product_Price=Product_Price);
					/*Ret_Records.Tot_Records = qry_validateProduct.RecordCount;
					Ret_Records.Dat_Records = qry_validateProduct;*/
					} else {
						ERR.ErrorFound = true;
						ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Sorry the product you are trying to add does not exist");
					}
				}
			}
		}
	</cfscript>
</cfsilent>
<cfif ERR.ErrorFound>
	<cfinclude template="./_tmp_account_default.cfm">
	<cfexit method="exittemplate">
<cfelse>
	<cfinclude template="./_tmp_cart_default.cfm">
</cfif>