<cfsilent>
	<cfparam name="VARIABLES.subTotal" default="0">
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getCart";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.Cart_Session_UID = SESSION.Session_UID;
		QUERY.Cart_User_Cookie = qry_getUser.User_Cookie;
		QUERY.Cart_IsCompleted = 0;
		QUERY.OrderBy = "cart.Cart_DTS";
	</cfscript>
	<cfinclude template="/_qry_select_cart_product.cfm">
</cfsilent>

<cfsavecontent variable="UpdateQuantity">
	<script type="text/javascript"> 
		function actionQuantity(field_id) {
			var upd_field = document.getElementById(field_id + "_upd");
			upd_field.style.visibility = "visible";
			
			var sav_field = document.getElementById(field_id + "_sav");
			sav_field.style.visibility = "hidden";
		}
		function htmlQuantity(data,field_id,qty,base_price) {
			var ret_result = "";
			$.each(data, function(index, data) {
				if(data.subtotal != "fail") {
					var sub_field = document.getElementById("subprice");
					sub_field.innerHTML = data.subtotal;
					
					var tot_field = document.getElementById(field_id + "_tot");
					field_total = qty * base_price;
					field_total = "$" + field_total.toFixed(2);//Simple Dollar format
					tot_field.innerHTML = field_total;
					
					var sav_field = document.getElementById(field_id + "_sav");
					sav_field.style.visibility = "visible";
					
					var upd_field = document.getElementById(field_id + "_upd");
					upd_field.style.visibility = "hidden";
					ret_result = "pass";
				} else {
					ret_result = "fail";
				}
			});
			
			return ret_result;
		}
		function updateQuantity(field_id,qty,base_price) {
			var jqty_act = "/js/qtyCart.cfm";
			$.ajax({
				url: jqty_act, 
				data: ({ sess: '<cfoutput>#SESSION.Session_UID#</cfoutput>', pdt: field_id, qty: qty }),
				dataType: "json",
				success: function(data) {
					var set_msg = htmlQuantity(data,field_id,qty,base_price);
					if(set_msg == "fail") {
						alert("Error occured");
						<cfoutput>
						window.location.href = "https://#CGI.HTTP_HOST#";
						</cfoutput>
					}
				}
			});
		}
		function confirmClear() {
			var $con_window = $('<div class="itemadded"></div>')
				.html('This will remove all of the items in your shopping cart. Are you sure?')
				.dialog({
					title: 'Confirm',
					modal:true,
					buttons: {
						"Remove All Items": function() {
							window.location = "./?pg=Cart&st=Clear";
						},
						Cancel: function() {
							$(this).dialog('close');
						}
					}
				});
		}
	</script>
</cfsavecontent>
<cfhtmlhead text="#UpdateQuantity#">

<cfoutput>
<div id="List" class="products clear">
	<h2>Shopping Cart</h2>
	<cfif qry_getCart.RecordCount NEQ 0>
	<form action="./?pg=Cart&st=Checkout" name="frm_cart" method="post" class="sscart">
	<table>
		<tr>
			<th>Quantity</th>
			<th>Product Name</th>
			<th>Supplier Part Number</th>
			<th>Manufacturer Part Number</th>
			<th>UOM</th>
			<th>Contract Price</th>
			<th>Total</th>
			<th></th>
		</tr>
		<cfloop query="qry_getCart"><cfset VARIABLES.subTotal = VARIABLES.subTotal + (Cart_Product_QTY * Cart_Product_Price_SubTotal)>
		<tr class="#IIF(CurrentRow MOD 2, DE('noshade'), DE('shade'))#">
			<td class="qty">
				<input onkeyup="return actionQuantity('#Product_UID#');" onchange="return actionQuantity('#Product_UID#');" type="text" name="cqty_#Product_UID#" value="#Cart_Product_QTY#">
				<div id="#Product_UID#_upd" class="status" style="visibility:hidden;"><a onclick="return updateQuantity('#Product_UID#',frm_cart.cqty_#Product_UID#.value,#Contract_Price#);" href="javascript:void(false);">Update</a></div>
				<div id="#Product_UID#_sav" class="status ok" style="visibility:hidden;">Saved</div>
			</td>
			<td>#Product_Short_Description#</td>
			<td>#Supplier_Part_Number#</td>
			<td>#Manufacturer_Part_Number#</td>
			<td>#UOM#</td>
			<td>#DollarFormat(Contract_Price)#</td>
			<td><div id="#Product_UID#_tot">#DollarFormat(Evaluate(Contract_Price*Cart_Product_QTY))#</div></td>
			<td class="remp"><a href="./?pg=Cart&st=Del&pdt=#Product_UID#">Delete</a></td>
		</tr></cfloop>
		<tr>
			<td colspan="8" class="cleartot">
				<div class="clearcart"><a href="javascript:void(false);" onclick="return confirmClear();">Clear Shopping Cart</a></div>
				<div class="subtotal">Subtotal:&nbsp;<span id="subprice">#DollarFormat(VARIABLES.subTotal)#</span></div>
			</td>
		</tr>
		<tr>
			<td colspan="4"><a href="./?pg=Search"><img src="/images/conshop_but.png" /></a></td>
			<td colspan="4" class="checkout"><input type="image" src="/images/cart_checkout.png" /></td>
		</tr>
	</table></form><cfelse>
	<table>
		<tr>
			<th>Quantity</th>
			<th>Product Name</th>
			<th>Supplier Part Number</th>
			<th>Manufacturer Part Number</th>
			<th>UOM</th>
			<th>Contract Price</th>
			<th></th>
		</tr>
		<tr>
			<td colspan="8"><h3>Your cart is empty</h3></td>
		</tr>
	</table></cfif>
	<div class="clear"></div>
	<cfinclude template="./_tmp_product_recommended.cfm">
</div>
</cfoutput>