<cfsilent>
<cfsetting showdebugoutput="no" enablecfoutputonly="no">
<cfparam name="URL.pdt" default="">
<cfparam name="URL.sess" default="">
<cfparam name="URL.qty" default="">
<cfparam name="badreq" default="false"> <!---set var to handle bad requests--->

<cfscript>
//Validation Step 1
	if(NOT StructKeyExists(SESSION, "Session_UID")) {
		badreq = true;
	}
	if(ReFindNoCase("[a-z0-9]{32}", URL.sess) EQ 0) {
		badreq = true;
	}
	if(ReFindNoCase("[a-z0-9]{32}", URL.pdt) EQ 0) {
		badreq = true;
	}
	if(NOT IsNumeric(URL.qty) OR URL.qty LT 1) {
		badreq = true;
	}
//Validation Step 2
	if(NOT badreq) {
		if(Compare(SESSION.Session_UID, URL.sess) NEQ 0) {//compare url to session to ensure match
			badreq = true;
		}
		initCartMgmt = CreateObject("component", "_cfcs.CartMgmt");
		validProduct = initCartMgmt.valItem(Product_UID=URL.pdt);
		if(validProduct.Tot_Records EQ 0) {
			badreq = true;
		}
	}
</cfscript>
</cfsilent>

<cfif NOT badreq>
<cfsilent>
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getCart";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.Cart_Session_UID = SESSION.Session_UID;
		QUERY.Cart_Product_UID = URL.pdt;
		QUERY.Cart_IsCompleted = 0;
		QUERY.OrderBy = "cart.Cart_DTS";
	</cfscript>
	<cfinclude template="/_qry_select_cart_product.cfm">
	
	<cfif qry_getCart.RecordCount NEQ 0>
		<cfquery name="qry_updateQty" datasource="#APPLICATION.Datasource#" maxrows="1">
			UPDATE stbl_User_Cart_Products
			SET Cart_Product_Qty = <cfqueryparam cfsqltype="cf_sql_integer" value="#URL.qty#">
			WHERE 
				Cart_Session_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#SESSION.Session_UID#">
			AND Cart_Product_UID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#URL.pdt#">
		</cfquery>
		
		<cfscript>
			QUERY = StructNew();
			QUERY.QueryName = "qry_getCart";
			QUERY.Datasource = APPLICATION.Datasource;
			QUERY.Cart_Session_UID = SESSION.Session_UID;
			QUERY.Cart_IsCompleted = 0;
			QUERY.OrderBy = "cart.Cart_DTS";
		</cfscript>
		<cfinclude template="/_qry_select_cart_product.cfm">
		
		<cfset subTotal = 0>
		<cfloop query="qry_getCart">
			<cfset subTotal = subTotal + (Cart_Product_QTY * Cart_Product_Price_SubTotal)>
		</cfloop>
	</cfif>	
</cfsilent>
<cfcontent type="text/plain" reset="yes">
<cfoutput>[{"subtotal":"#DollarFormat(subTotal)#"}]</cfoutput>
<cfelse>
<cfcontent type="text/plain" reset="yes">
<cfoutput>
[{"subtotal":"fail"}]
</cfoutput>
</cfif>
<cfabort>