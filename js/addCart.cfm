<cfsilent>
<cfsetting showdebugoutput="no" enablecfoutputonly="no">
<cfparam name="URL.pdt" default="">
<cfparam name="URL.sess" default="">
<cfparam name="URL.qty" default="1">
<cfparam name="URL.uco" default="">
<cfparam name="badreq" default="false"> <!---set var to handle bad requests--->

<cfscript>
//Validation Step 1
	if(NOT StructKeyExists(SESSION, "Session_UID")) {
		badreq = true;
	}
	if(ReFindNoCase("[a-z0-9]{32}", URL.sess) EQ 0) {
		badreq = true;
	}
	if(ReFindNoCase("[a-z0-9]{32}", URL.uco) EQ 0) { //User Cookie Need To Validate against Session
		badreq = true;
	}
	if(ReFindNoCase("[a-z0-9]{32}", URL.pdt) EQ 0) {
		badreq = true;
	}
	if(NOT IsNumeric(URL.qty) OR URL.qty LT 1) {
		badreq = true;
	}
//Validation Step 2
	if(NOT badreq) {
		if(Compare(SESSION.Session_UID, URL.sess) NEQ 0) {//compare url to session to ensure match
			badreq = true;
		}
		initCartMgmt = CreateObject("component", "_cfcs.CartMgmt");
		validProduct = initCartMgmt.valItem(Product_UID=URL.pdt);
		
		if(validProduct.Tot_Records EQ 0) {
			badreq = true;
		} else { //Add to Cart
			Product_Price = validProduct.Dat_Records.Contract_Price; //Set Contract Price
			addProduct = initCartMgmt.addItem(Session_UID=SESSION.Session_UID,User_Cookie=URL.uco,Product_UID=URL.pdt,Product_QTY=URL.qty,Product_Price=Product_Price);
		}
	}
</cfscript>

		
</cfsilent>

<cfif NOT badreq>
<cfsilent>
	<cfscript>
		QUERY = StructNew();
		QUERY.QueryName = "qry_getCart";
		QUERY.Datasource = APPLICATION.Datasource;
		QUERY.Cart_Session_UID = SESSION.Session_UID;
		QUERY.Cart_IsCompleted = 0;
		QUERY.OrderBy = "cart.Cart_DTS";
	</cfscript>
	<cfinclude template="/_qry_select_cart_product.cfm">
	
	<cfif qry_getCart.RecordCount NEQ 0>
		<cfset Cart_Total = qry_getCart.RecordCount>
	<cfelse>
		<cfset Cart_Total = 0>
	</cfif>	
</cfsilent>
<cfcontent type="text/plain" reset="yes">
<cfoutput>[{"carttotal":"#Cart_Total#"}]</cfoutput>
<cfelse>
<cfcontent type="text/plain" reset="yes">
<cfoutput>[{"carttotal":"fail"}]</cfoutput>
</cfif>
<cfabort>