<cfsilent>
	<cfparam name="FORM.pdt" default="">
	<cfparam name="FORM.cqty" default="0">
	<cfparam name="ERR" default="#StructNew()#">
	<cfparam name="ERR.ErrorFound" default="false">
	<cfparam name="ERR.ErrorMessage" default="">
	
	<!---Validation and insertion--->
	<cfscript>
		if(ReFindNoCase("[a-z0-9]{32}", FORM.pdt) EQ 0) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Sorry the product you are trying to add does not exist");
		}
		if(NOT IsNumeric(FORM.cqty) OR FORM.cqty LT 1) {
			ERR.ErrorFound = true;
			ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Please select a valid quantity");
		}
		if(NOT ERR.ErrorFound) {
			manageCart = CreateObject("component", "_cfcs.CartMgmt");
			valProduct = manageCart.valItem(Product_UID=FORM.pdt); //Validate Product and retrieve record
			
			if(valProduct.Tot_Records NEQ 0) { //Meaning that this an active and valid product
				Product_Price = valProduct.Dat_Records.Contract_Price; //Set Contract Price
				
				addProduct = manageCart.addItem(Session_UID=SESSION.Session_UID,User_Cookie=qry_getUser.User_Cookie,Product_UID=FORM.pdt,Product_QTY=FORM.cqty,Product_Price=Product_Price);
			/*Ret_Records.Tot_Records = qry_validateProduct.RecordCount;
			Ret_Records.Dat_Records = qry_validateProduct;*/
			} else {
				ERR.ErrorFound = true;
				ERR.ErrorMessage = ListAppend(ERR.ErrorMessage, "Sorry the product you are trying to add does not exist");
			}
		}
	</cfscript>
</cfsilent>

<cfif ERR.ErrorFound>
	<cfset URL.pdt = FORM.pdt>
	<cfinclude template="./_tmp_product_detail.cfm">
	<cfexit method="exittemplate">
<cfelse>
	<cfinclude template="./_tmp_cart_default.cfm">
</cfif>