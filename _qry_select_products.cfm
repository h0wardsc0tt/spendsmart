<cfparam name="QUERY" default="#StructNew()#">
<cfparam name="QUERY.QueryName" default="qry_getProducts">
<cfparam name="QUERY.Datasource" default="#APPLICATION.Datasource#">
<cfparam name="QUERY.OrderBy" default="prods.Product_Short_Description">

<cfquery name="#QUERY.QueryName#" datasource="#QUERY.Datasource#">
	SELECT prods.*
	FROM tbl_Products prods
	WHERE 0=0
	<cfif StructKeyExists(QUERY, "Product_IsActive")>
	AND Product_IsActive IN (<cfqueryparam cfsqltype="cf_sql_tinyint" value="#QUERY.Product_IsActive#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Product_UID")>
	AND Product_UID IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Product_UID#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Product_UID_NOT_IN")>
	AND Product_UID NOT IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Product_UID_NOT_IN#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Manufacturer_Part_Number")>
	AND Manufacturer_Part_Number IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Manufacturer_Part_Number#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Manufacturer_Part_Number_LIKE")>
	AND Manufacturer_Part_Number LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.Manufacturer_Part_Number_LIKE#%">
	</cfif>
	<cfif StructKeyExists(QUERY, "Supplier_Part_Number")>
	AND Supplier_Part_Number IN (<cfqueryparam cfsqltype="cf_sql_varchar" value="#QUERY.Supplier_Part_Number#" list="yes">)
	</cfif>
	<cfif StructKeyExists(QUERY, "Manufacturer_Part_Number_LIKE")>
	AND Supplier_Part_Number LIKE <cfqueryparam cfsqltype="cf_sql_varchar" value="%#QUERY.Supplier_Part_Number_LIKE#%">
	</cfif>
	ORDER BY #QUERY.OrderBy#
</cfquery>

<cfquery name="#QUERY.QueryName#_Total" dbtype="query">
	SELECT Count(*) AS Records
	FROM #QUERY.QueryName#
</cfquery>

<cfif StructClear(QUERY)></cfif>